const express=require('express');
const http=require('http');
const app=express(http);
const MongoClient=require('mongodb').MongoClient;
const body_parser=require('body-parser');

app.use(body_parser.urlencoded({ extended: false }))
app.use(body_parser.json())


app.post('/user',(req,res)=>{

	let request_body=req.body;
	MongoClient.connect('mongodb://localhost:27017',{ useNewUrlParser: true },(err,client)=>{
		if(!err){
			const db=client.db('employee_test');
			const collection=db.collection('users');
			collection.count((err,count)=>{
				console.log(err,count);
				if(!err){
				request_body.id=++count;
				request_body.createdAt=new Date().getTime();
			collection.save(request_body,(err,doc)=>{
				if(!err)
					{
						console.log(` User saved with id ${request_body.id} `);
						collection.findOne({id:request_body.id},(err,user)=>{
							console.log(user);
							res.send({
								status:'success',
								data:user,
								message:req.protocol + '://' + req.get('host') + req.originalUrl
							})
						});
					}
				else{
					console.log(err);
					res.send(err);
				}
				}
			);
		}
		else{
			console.log(err);
			res.send(err);
		}
			});
		}
			
		else{
			console.log(err);
			res.send(err);
		}

	})
		});

app.get('/user',(req,res)=>{
  let request_id=parseInt(req.query.id);
  console.log(request_id);
  MongoClient.connect('mongodb://localhost:27017',{ useNewUrlParser: true },(err,client)=>{
    const db=client.db('employee_test');
    const collection=db.collection('users');
    collection.findOne({id:request_id},(err,user)=>{
      if(!err){
      console.log(user,err)
      res.send(user);

    }
    else 
      res.send(err);;
    })
  })
});

app.listen(3000,()=>{
	console.log('Server listening at 3000')
})